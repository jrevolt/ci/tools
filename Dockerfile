FROM alpine as download
WORKDIR /tmp
RUN wget https://github.com/anchore/grype/releases/download/v0.38.0/grype_0.38.0_linux_amd64.tar.gz && tar xzvf *tar.gz

FROM alpine as grype
COPY --from=download /tmp/grype /usr/local/bin/
RUN grype db update -vv

